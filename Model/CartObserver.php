<?php

namespace Fanplayr\SmartAndTargeted\Model;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\Http;
use Psr\Log\LoggerInterface;

use Fanplayr\SmartAndTargeted\Helper\Data as HelperData;

class CartObserver implements ObserverInterface
{
  protected $_logger;
  protected $_request;
  protected $_helper;

  public function __construct (
    HelperData $helper,
    LoggerInterface $logger,
    Http $request,
    array $data = []
  )
  {
    $this->_logger = $logger;
    $this->_request = $request;
    $this->_helper = $helper;
  }

  private function log( $message ) {
    $this->_logger->addDebug($message);
  }

  public function execute(\Magento\Framework\Event\Observer $observer)
  {
    $eventName = $observer->getEvent()->getName();

    //$this->log('CartObserver - Event Name: ' . $eventName);

    // cart has been updated
    if ( $eventName == 'checkout_cart_save_after' ) {

      $cart = $observer['cart'];
      $this->_helper->refreshCartDetails($cart->getQuote());

    }

    // seems it's only for capturing and allowing us to cancel a coupon?
    // may be useful later ...
    if ( $eventName == 'salesrule_validator_process' ) {

      // $this->_helper->refreshCartDetails();

    }

    // maybe this will allow us to update once we've added a coupon?
    // who knows anymore ...
    if ( $eventName == 'checkout_cart_update_items_after' ) {

      $this->_helper->refreshCartDetails();

    }

    // we have a new quote
    //
    // this could be because we have a new session
    // this does mean it's clear though, as if there is a quote for that user
    // then it simply loads the quote, and doesn't dispatch an event ...

    // it also calls init when you log into an account that has a quote
    // so that's why we need to make sure it's not empty
    if ( $eventName == 'checkout_quote_init' ) {

      $quote = $observer['quote'];
      $this->_helper->refreshCartDetails($quote);

    }

    // not really sure what this one actually does
    // but it should be here just in case no ?
    if ( $eventName == 'checkout_type_onepage_save_order_after' ) {

      $quote = $observer['quote'];
      $this->_helper->refreshCartDetails($quote);

    }

    if ( $eventName == 'controller_action_postdispatch_checkout_cart_couponpost' ) {

      $this->_helper->refreshCartDetails();

    }

    if ( $eventName == 'customer_login' ) {

      $this->_helper->refreshCartDetailsNextCall();

    }

    if ( $eventName == 'customer_logout' ) {

      $this->_helper->refreshCartDetails();

    }

    if ( $eventName == 'checkout_onepage_controller_success_action' ) {
      // array of single order ID
      $this->_helper->setLastOrderDetails([$observer['order_ids']]);
    }

    if ( $eventName == 'multishipping_checkout_controller_success_action' ) {
      // array of order IDs
      $this->_helper->setLastOrderDetails([$observer['order_ids']]);
    }

    // example: logout
    // on logout at least, 'init' is called again anyway so meh ...
    // maybe we don't need this ?
    if ( $eventName == 'checkout_quote_destroy' ) {

      // $this->_helper->clearCartDetails($quote);

    }


  }

}