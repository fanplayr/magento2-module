<?php

namespace Fanplayr\SmartAndTargeted\Model;

use Magento\Framework\Event\ObserverInterface;
//use Magento\Framework\App\Request\Http;
use Magento\Framework\App\RequestInterface;
use Psr\Log\LoggerInterface;

use Fanplayr\SmartAndTargeted\Helper\Data as HelperData;

class CouponObserver implements ObserverInterface
{
  protected $logger;
  protected $request;
  protected $helper;
  protected $session;

  public function __construct (
    HelperData $helper,
    LoggerInterface $logger,
    RequestInterface $request,
    array $data = []
  )
  {
    $this->helper = $helper;
    $this->logger = $logger;
    $this->request = $request;

    $this->helper->init();
    $this->session = $this->helper->getSession();
  }

  private function log( $message ) {
    $this->logger->addDebug($message);
  }

  public function execute(\Magento\Framework\Event\Observer $observer)
  {
    // DEBUG
    $this->helper->log('CouponObserver/execute() SESSION ID: ' . $this->session->getSessionId());

    $eventName = $observer->getEvent()->getName();

    //$this->log('CartObserver - Event Name: ' . $eventName);

    // before applying coupon
    if ( $eventName == 'controller_action_predispatch_checkout_cart_couponpost' ) {

      $couponCode = $this->request->getParam('coupon_code');

      // -----------------------------------------
      // check session offer
      $sessionOfferPrefix = $this->helper->getSessionOfferPrefix();

      if ( $sessionOfferPrefix && $sessionOfferPrefix !== null && $couponCode !== null) {
        if ( strpos(strtoupper($couponCode), strtoupper($sessionOfferPrefix)) === 0 ) {
          // get current valid session offers
          try {
            $sessionOffersValidated = json_decode($this->session->getData('fanplayr_session_offers'));
          } catch(\Exception $e) {
            $sessionOffersValidated = array();
          }
          if (!is_array($sessionOffersValidated)) {
            $sessionOffersValidated = array();
          }

          if (array_search(strtoupper($couponCode), $sessionOffersValidated) === false ) {
            // make the coupon not work by adding a weird invisible space UTF character
            $this->request->setParam('coupon_code', mb_convert_encoding('&#x180E;', 'UTF-8', 'HTML-ENTITIES') . $couponCode);
          }
        }
      }

    }

  }

}