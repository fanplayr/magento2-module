<?php

namespace Fanplayr\SmartAndTargeted\Controller\GetEmbedJs;

//use Magento\Framework\View\Result\PageFactory;

use Fanplayr\SmartAndTargeted\Helper\Data as HelperData;

class Index extends \Magento\Framework\App\Action\Action
{
    //protected $_response;
    //protected $_request;

    public function __construct(
        \Magento\Framework\App\Action\Context $context
    ){
        parent::__construct($context);
    }

    private function fpqr($s)
    {
      if ($s !== null) {
        $s = str_replace("'", "\'", $s);
        $s = str_replace("<", "&lt;", $s);
        $s = str_replace(">", "&gt;", $s);
      }
      return $s;
    }

    public function execute()
    {
        $helper = $this->_objectManager->create('Fanplayr\SmartAndTargeted\Helper\Data');
        $helper->init();

        // DEBUG
        $helper->log('GetEmbedJs/execute() SESSION ID: ' . $helper->getSession()->getSessionId());
        $helper->log('GetEmbedJs/execute() VALIDATED: ' . $helper->getSession()->getData('fanplayr_session_offers'));

        // debug last order
        //var_dump($helper->setLastOrderDetails());

        $params = $this->getRequest()->getParams();

        $pageType = $params['pageType'];

        $categoryId = $params['categoryId'];
        $categoryName = $params['categoryName'];

        $productId = $params['productId'];
        $productName = $params['productName'];
        $productSku = $params['productSku'];
        $productPrice = $params['productPrice'];
        $productUrl = $params['productUrl'];
        $productImage = $params['productImage'];

        // --------------------------
        $accountKey = $helper->getAccountKey();
        $storeDomain = $helper->getStoreDomain();

        $lineItemCount = 0;
        $numItems = 0;
        $subTotal = 0;
        $total = 0;
        $discount = 0;
        $discountCode = '';
        $currency = '';
        $products = '';

        $customerId = '';
        $customerEmail = '';
        $customerFirstname = '';
        $customerLastname = '';

        $cartDetails = $helper->getCartDetails();

        if ($cartDetails && is_object($cartDetails)) {
          $lineItemCount = $cartDetails->lineItemCount;
          $numItems = $cartDetails->numItems;
          $subTotal = $cartDetails->subTotal;
          $total = $cartDetails->total;
          $discount = $cartDetails->discount;

          $discountCode = $this->fpqr($cartDetails->discountCode);
          $currency = $this->fpqr($cartDetails->currency);
          $products = $this->fpqr($cartDetails->products);

          $customerId = $this->fpqr($cartDetails->id);
          $customerEmail = $this->fpqr($cartDetails->email);
        }

        $applyUrl = $this->_url->getUrl('fpsmart/coupon/apply');
        $sessionUrl = $this->_url->getUrl('fpsmart/coupon/session');
        $scriptKey = "";
        $customLoader = "'https://cdn.fanplayr.com/client/production/loader.js?' + (new Date().getTime())";

        if ($helper->getConfig('fanplayr/privacyid/enabled')){
          $scriptKey = $helper->getConfig('fanplayr/privacyid/scriptKey');
        } 
        $connectUrl = $this->_url->getUrl('fpsmart/connect');

        if ($helper->getConfig('fanplayr/extra/customloader')){
          $customLoader = "'" . $helper->getConfig('fanplayr/extra/customloader') . "'";
        }

        $js = <<<EOD
(function(d, w, s) {
  var f = w.fanplayr = w.fanplayr || { _i:[] };
  f.custom = f.custom || {};
  var _ = f.custom;
  f._i.push({
    type: 'st',
    accountKey: _.accountKey || '$accountKey',
    storeDomain: _.storeDomain || '$storeDomain',
    privacyId: '$scriptKey',
    applyToCartUrl: encodeURIComponent(_.applyToCartUrl || '$applyUrl?coupon_code=%c'),
    sessionOfferUrl: encodeURIComponent(_.sessionOfferUrl || '$sessionUrl?coupon_code=%c'),
    connect: {
      endpoint: _.connectUrl || '$connectUrl'
    },
    data: {
      cartAction: 'override',

      pageType: '$pageType',

      categoryId: '$categoryId',
      categoryName: '$categoryName',
      productId: '$productId',
      productName: '$productName',
      productSku: '$productSku',
      productPrice: $productPrice,
      productUrl: '$productUrl',
      productImage: '$productImage',

      lineItemCount: $lineItemCount,
      numItems: $numItems,
      subTotal: $subTotal,
      total: $total,
      discount: $discount,
      discountCode: '$discountCode',
      currency: '$currency',
      products: '$products',

      customerId: '$customerId',
      customerEmail: '$customerEmail',

      shopType: 'magento2',
      version: 3  // Mandatory
    }
  });

  var js = d.createElement(s);
  var fjs = d.getElementsByTagName(s)[0];
  js.async = true;
  js.src = $customLoader;
  fjs.parentNode.insertBefore(js, fjs);
})(document, window, 'script');


EOD;

        $response = $this->getResponse();
        $response->clearHeaders();
        $response->setHeader('Cache-Control', 'no-cache, must-revalidate');
        $response->setHeader('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
        $response->setHeader('Content-Type', 'application/javascript');
        $response->setBody($js);
    }
}