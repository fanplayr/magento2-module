<?php

namespace Fanplayr\SmartAndTargeted\Controller\Notifications;

class Index extends \Magento\Framework\App\Action\Action
{
  /**
   * @var \Magento\Framework\Controller\Result\JsonFactory
   */
  protected $resultPageFactory;
  /**
   * @param \Magento\Framework\App\Action\Context $context
   * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
   */
  public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
  ) {
    parent::__construct($context);
    $this->resultJsonFactory = $resultJsonFactory;
  }
  /**
   * View  page action
   *
   * @return \Magento\Framework\Controller\ResultInterface
   */
  public function execute()
  {
    $response = $this->getResponse();
    $response->clearHeaders();
    $response->setHeader('Cache-Control', 'no-cache, must-revalidate');
    $response->setHeader('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
    $response->setHeader('Content-Type', 'text/javascript');
    $response->setHeader('Service-Worker-Allowed', '/');
    $response->setBody("importScripts('https://static.fanplayr.com/client/sw.js');");
  }
}