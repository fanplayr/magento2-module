<?php

namespace Fanplayr\SmartAndTargeted\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
  /**
   * @var \Magento\Framework\Controller\Result\JsonFactory
   */
  protected $resultPageFactory;
  /**
   * @param \Magento\Framework\App\Action\Context $context
   * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
   */
  public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
  ) {
    parent::__construct($context);
    $this->resultJsonFactory = $resultJsonFactory;
  }
  /**
   * View  page action
   *
   * @return \Magento\Framework\Controller\ResultInterface
   */
  public function execute()
  {
    $resultJsonFactory = $this->resultJsonFactory->create();
    $resultJsonFactory->setData(['error' => true, 'message' => 'Invalid API call.', 'module' => 'fanplayr', 'version' => $this->_objectManager->create('Fanplayr\SmartAndTargeted\Helper\Data')->getVersion()]);
    return $resultJsonFactory;
  }
}