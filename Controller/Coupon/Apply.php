<?php

namespace Fanplayr\SmartAndTargeted\Controller\Coupon;

class Apply extends \Magento\Framework\App\Action\Action
{
  protected $result;
  protected $response;
  protected $checkoutSession;
  protected $helper;
  protected $session;

  protected $quoteRepository;
  protected $couponFactory;
  /**
   * @param \Magento\Framework\App\Action\Context $context
   * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
   * @param \Magento\Checkout\Model\Session $checkoutSession
   */
  public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
    \Magento\Checkout\Model\Session $checkoutSession,
    \Magento\Framework\App\ResponseFactory $responseFactory,
    \Magento\Catalog\Model\Session $session,
    \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
    \Magento\Checkout\Model\Cart $cart,
    \Magento\SalesRule\Model\CouponFactory $couponFactory
  ) {
    parent::__construct($context);
    $this->result = $resultJsonFactory->create();
    $this->response = $responseFactory->create();
    $this->checkoutSession = $checkoutSession;
    $this->couponFactory = $couponFactory;
    $this->session = $session;
    
    $this->quoteRepository = $quoteRepository;
    $this->cart = $cart;
    $this->quoteRepository = $quoteRepository;

    $this->helper = $this->_objectManager->create('Fanplayr\SmartAndTargeted\Helper\Data');
    $this->helper->init();
  }
  /**
   * View  page action
   *
   * @return \Magento\Framework\Controller\ResultInterface
   */
  public function execute()
  {
    // DEBUG
    $this->helper->log('Apply/execute() SESSION ID: ' . $this->session->getSessionId());

    // used for messages
    $escaper = $this->_objectManager->get('Magento\Framework\Escaper');

    $couponCode = $this->getRequest()->getParam('coupon_code');

    // -----------------------------------------
    // check session offer
    $sessionOfferPrefix = $this->helper->getSessionOfferPrefix();

    $this->helper->log('Apply/execute() OFFER PREFIX: ' . $sessionOfferPrefix);

    if ( $sessionOfferPrefix ) {
        if ( strpos(strtoupper($couponCode), strtoupper($sessionOfferPrefix)) === 0 ) {
            // get current valid session offers
            try {
              $sessionOffersValidated = json_decode($this->session->getData('fanplayr_session_offers'));
              $this->helper->log('Apply/execute() VALIDATED: ' . json_encode($sessionOffersValidated));
            } catch(\Exception $e) {
              $sessionOffersValidated = array();
            }
            if (!is_array($sessionOffersValidated)) {
              $sessionOffersValidated = array();
            }

            $this->helper->log('Apply/execute() COUPON-CODE: ' . json_encode(strtoupper($couponCode)));

            if (array_search(strtoupper($couponCode), $sessionOffersValidated) === false ) {
                $this->messageManager->addError(
                    __(
                        'The coupon code "%1" is not valid. (S)',
                        $escaper->escapeHtml($couponCode)
                    )
                );
                $couponCode = '';
                $this->redirect();
                return;
            }
        }
    }

    // -----------------------------------------
    // apply
    // based on /Magento/Checkout/Controller/Cart/CouponPost
    $cartQuote = $this->checkoutSession->getQuote();
    
    $codeLength = strlen($couponCode);
    try {
        $isCodeLengthValid = $codeLength && $codeLength <= \Magento\Checkout\Helper\Cart::COUPON_CODE_MAX_LENGTH;

        $itemsCount = $cartQuote->getItemsCount();
        if ($itemsCount) {
            $cartQuote->getShippingAddress()->setCollectShippingRates(true);
            $cartQuote->setCouponCode($isCodeLengthValid ? $couponCode : '')->collectTotals();
            $this->quoteRepository->save($cartQuote);
        }

        if ($codeLength) {
            if (!$itemsCount) {
                if ($isCodeLengthValid) {
                    $coupon = $this->couponFactory->create();
                    $coupon->load($couponCode, 'code');
                    if ($coupon->getId()) {
                        $cartQuote->setCouponCode($couponCode)->save();
                        $this->messageManager->addSuccess(
                            __(
                                'You used coupon code "%1".',
                                $escaper->escapeHtml($couponCode)
                            )
                        );
                    } else {
                        $this->messageManager->addError(
                            __(
                                'The coupon code "%1" is not valid.',
                                $escaper->escapeHtml($couponCode)
                            )
                        );
                    }
                } else {
                    $this->messageManager->addError(
                        __(
                            'The coupon code "%1" is not valid.',
                            $escaper->escapeHtml($couponCode)
                        )
                    );
                }
            } else {
                if ($isCodeLengthValid && $couponCode == $cartQuote->getCouponCode()) {
                    $this->messageManager->addSuccess(
                        __(
                            'You used coupon code "%1".',
                            $escaper->escapeHtml($couponCode)
                        )
                    );
                } else {
                    $this->messageManager->addError(
                        __(
                            'The coupon code "%1" is not valid.',
                            $escaper->escapeHtml($couponCode)
                        )
                    );
                    $this->cart->save();
                }
            }
        } else {
            $this->messageManager->addSuccess(__('You canceled the coupon code.'));
        }
    } catch (\Magento\Framework\Exception\LocalizedException $e) {
        $this->messageManager->addError($e->getMessage());
    } catch (\Exception $e) {
        var_dump($e->getMessage());
        $this->messageManager->addError(__('We cannot apply the coupon code.'));
        $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
    }

    $this->redirect();
  }

  private function returnData($error, $message) {
    $this->result->setData(['error' => $error, 'method' => 'apply', 'message' => $message, 'module' => 'fanplayr', 'version' => $this->helper->getVersion()]);
    return $this->result;    
  }

  private function redirect() {
    $cartUrl = $this->_url->getUrl('checkout/cart/index');
    $this->_redirect($cartUrl);
  }
}