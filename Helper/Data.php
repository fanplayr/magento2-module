<?php
namespace Fanplayr\SmartAndTargeted\Helper;

use Magento\Framework\App\ObjectManager;
//use Psr\Log\LoggerInterface;
use Magento\Framework\App\Helper\Context;
//use Magento\Framework\App\RequestInterface;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Session;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Sales\Model\Order;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
  private $_initted = false;
  protected $_accountKey;
  protected $_sessionOfferPrefix;
  protected $_storeCartInPhpSession;

  protected $_request;
  protected $_registry;
  protected $_storeManager;
  protected $_categoryRepository;
  protected $_logger;
  protected $_session;
  protected $_customerSession;
  protected $_order;

  public function __construct(
    Context $context,
    /*RequestInterface $request,*/
    Registry $registry,
    StoreManagerInterface $storeManager,
    CategoryRepositoryInterface $categoryRepository,
    /*LoggerInterface $logger,*/
    Session $session,
    CustomerSession $customerSession,
    Order $order
  ) {
    parent::__construct($context);

    //$this->_request = $request;
    $this->_request = $context->getRequest();
    $this->_registry = $registry;
    $this->_storeManager = $storeManager;
    $this->_categoryRepository = $categoryRepository;
    //$this->_logger = $logger;
    $this->_logger = $context->getLogger();
    $this->_session = $session;
    $this->_customerSession = $customerSession;
    $this->_order = $order;

    $this->_storeCartInPhpSession = $this->getConfig('fanplayr/options/store_cart_in_php_session');
  }

  public function getVersion() {
    return '1.5.0';
  }

  public function log( $message ) {
    // DEBUG ONLY
    // $this->_logger->addDebug($message);
  }

  public function getRequest() {
    return $this->_request;
  }

  public function init()
  {
    if ($this->_initted) {
      return;
    }
    $this->_accountKey = $this->getConfig('fanplayr/account/key');
    $this->_sessionOfferPrefix = $this->getConfig('fanplayr/session/prefix');
    
    $this->_initted = true;
    $this->checkIfCartDetailsRefreshNeeded();
  }

  public function isEnabled()
  {
    return !!$this->_accountKey;
  }

  public function getAccountKey()
  {
    return $this->_accountKey;
  }

  public function getSessionOfferPrefix()
  {
    return $this->_sessionOfferPrefix;
  }

  public function getSession()
  {
    return $this->_session;
  }

  public function getConfig($config_path)
  {
    return $this->scopeConfig->getValue(
      $config_path,
      \Magento\Store\Model\ScopeInterface::SCOPE_STORE
    );
  }

  public function setSessionData($key, $value)
  {
    if ($this->_storeCartInPhpSession) {
      $_SESSION['fanplayr__' . $key] = $value;
    } else {
      $this->_session->setData($key, $value);
    }
  }

  public function getSessionData($key)
  {
    if ($this->_storeCartInPhpSession) {
      if (array_key_exists('fanplayr__' . $key, $_SESSION)) {
        return $_SESSION['fanplayr__' . $key];
      } else {
        return null;
      }
    } else {
      return $this->_session->getData($key);
    }
  }

  public function getCurrentUrl()
  {
    return $this->_urlBuilder->getCurrentUrl();
  }

  public function getBaseUrl()
  {
    return $this->_storeManager->getStore()->getBaseUrl();
  }

  public function getStoreDomain()
  {
    $storeDomain = $this->getEmbedUrl();
    $storeDomain = substr($storeDomain, strpos($storeDomain, '//') + 2);
    $storeDomain = substr($storeDomain, 0, strpos($storeDomain, '/'));
    return $storeDomain;
  }

  public function getEmbedUrl() {
    $embedUrl = $this->getBaseUrl();
    if (strpos($embedUrl, 'index.php/') === false)
      $embedUrl .= 'index.php/';
    $customEmbedUrl = $this->getConfig('fanplayr/options/custom_embed_url');
    if (!empty($customEmbedUrl)) $embedUrl = $customEmbedUrl;

    // make sure that the shop URL is protocol agnostic
    if (strpos($embedUrl, '//') !== false) {
      $embedUrl = substr($embedUrl, strpos($embedUrl, '//'));
    }

    return $embedUrl;
  }

  public function getBaseMediaUrl()
  {
    return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
  }

  public function getCategoryById( $id )
  {
    $category = null;
    try {
      $category = $this->_categoryRepository->get($id, $this->_storeManager->getStore()->getId());
    } catch(\Exception $e){
      $this->_logger->addError($e->getMessage());
    }
    return $category;
  }

  public function getCurrentProduct()
  {
    return $this->_registry->registry('current_product');
  }

  public function getCurrentCategory()
  {
    return $this->_registry->registry('current_category');
  }

  public function addSlash( $str ) {
    if ($str !== null) {
      return str_replace("'", "\\'", $str);
    }
  }

  public function getPageType()
  {
    // pageType
    // home, cart, page, srch, cat, prod, blog
    $pController = $this->_request->getControllerName();
    $pAction = $this->_request->getActionName();
    $pRate = $this->_request->getRouteName();
    // $pModule = $this->_request->getModuleName();

    // home
      // index, index, cms, cms
    if ($pController == 'index' && $pAction == 'index' && $pRate == 'cms') {
      return 'home';
    }

    // product
      // product, view, catalog, catalog
    if ($pController == 'product') {
      return 'prod';
    }

    // category
      // category, view, catalog, catalog
    if ($pController == 'category') {
      return 'cat';
    }

    // search
      // result, index, catalogsearch, catalogsearch
      // advanced, index, catalogsearch, catalogsearch
      // term, popular, catalogsearch, catalogsearch
      // seo_sitemap, category, catalog, catalog
    if ($pController == 'result' || $pController == 'advanced' || $pController == 'term' || $pController == 'seo_sitemap') {
      return 'srch';
    }

    // cart
      // cart, index, checkout, checkout
    if ($pController == 'cart') {
      return 'cart';
    }

    return 'page';
  }

  private function getClearCartDetails()
  {
    // at least we can see what the current currency is
    $currency = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();

    // and if they're logged in the current user details
    $customerEmail = '';
    $customerId = '';
    //$customerFirstName = '';
    //$customerLastName = '';
    //
    if($this->_customerSession && $this->_customerSession->isLoggedIn()) {
      $user = $this->_customerSession->getUser();
      if ( $user ) {
        $customerEmail = $user->getEmail();
        $customerId = $user->getUserId();
        //$customerFirstName = $user->getFirstname();
        //$customerLastName = $user->getLastname();
      }
    }

    $cartDetails = array(
      'lineItemCount' => 0,
      'numItems' => 0,

      'subTotal' => 0,
      'total' => 0,
      'discount' => 0,
      'discountCode' => '',

      'currency' => $currency,
      'products' =>  '[]',

      'id' => $customerId,
      'email' => $customerEmail
      //'first' => $customerFirstName,
      //'last' => $customerLastName
    );

    return $cartDetails;
  }

  public function clearLastOrder()
  {
    $this->setSessionData('fanplayr_last_order', null);
  }

  public function getLastOrderDetails()
  {
    return (object) $cartDetails = json_decode($this->getSessionData('fanplayr_last_order'));
  }

  public function setLastOrderDetails($orderIds = null)
  {
    if ( !is_array($orderIds) ) {
      // this should always be an array, even for a single order
      // return;
      $orderIds = json_decode($this->getSessionData('fanplayr_last_order_ids'));
    } else {
      // just for testing really ...
      $this->setSessionData('fanplayr_last_order_ids', json_encode($orderIds));
    }

    $orderDetails = array(
      'orderId' => '',
      'orderNumber' => '',
      'quoteId' => '',
      'subTotal' => 0,
      'total' => 0,
      'discount' => 0,
      'discountCode' => '',
      'shipping' => 0,
      'tax' => 0,
      'currency' => '',
      'orderEmail' => '',
      'firstName' => '',
      'lastName' => '',
      'customerEmail' => '',
      'customerId' => '',
      'products' => '[]'
    );

    $products = array();

    foreach( $orderIds as $orderId ) {

      // get each order ...
      $order = $this->_order->load($orderId);

      // only allow a single for most of these ... umm ?
      $orderDetails['orderId'] = $order->getId();
      $orderDetails['orderNumber'] = $order->getRealOrderId();
      $orderDetails['quoteId'] = $order->getQuoteId();
      $orderDetails['orderEmail'] = $order->getCustomerEmail();
      $orderDetails['date'] = $order->getCreatedAt();
      $orderDetails['currency'] = $order->getOrderCurrencyCode();
      $orderDetails['firstName'] = $order->getCustomerFirstname();
      $orderDetails['lastName'] = $order->getCustomerLastname();
      $orderDetails['customerEmail'] = $order->getCustomerEmail();
      $orderDetails['customerId'] = $order->getCustomerId();
      $orderDetails['discountCode'] = $order->getCouponCode();
      if (!$orderDetails['discountCode']) $orderDetails['discountCode'] = '';

      // these details we add to one another ...
      $data = $order->getData();

      $orderDetails['discount'] += abs(array_key_exists('base_discount_amount', $data) ? round($data['base_discount_amount'], 2) : 0);

      $orderDetails['subTotal'] += round($order->getSubtotal(), 2);
      $orderDetails['total'] += $orderDetails['subTotal'] - abs($orderDetails['discount']);

      $orderDetails['shipping'] += round(abs($order->getShippingAmount()) + abs($order->getShippingTaxAmount()), 2);
      $orderDetails['tax'] += round($order->getTaxAmount(), 2);

      // and simply add to the products array ...
      $items = $order->getAllItems();
      foreach ($items as $itemId => $item) {
        if ($item->getQtyOrdered() > 0 && !$item->getParentItemId()){
          $products[] = array(
            'qty' => intval($item->getQtyOrdered()),
            'id' => $item->getProductId(),
            'sku' => $item->getSku(),
            'name' => $item->getName(),
            'price' => floatval($item->getPrice()),
            'catId' => '',
            'catName' => ''
          );
        }
      }
    }

    $orderDetails['products'] = $products;

    $this->setSessionData('fanplayr_last_order', json_encode($orderDetails));

    // TESTING
    return $orderDetails;
  }

  public function clearCart()
  {
    $cartDetails = $this->getClearCartDetails();
    $this->setCartDetails($cartDetails);
  }

  public function getCartDetails( )
  {
    $tempCartDetails = $this->getSessionData('fanplayr_cart');
    if ($tempCartDetails !== null) {
      $cartDetails = json_decode($tempCartDetails);
      // if there has been no cart interaction with the session we won't have anything
      // so let's just give them empty details
      if ( !is_object($cartDetails) || $cartDetails->lineItemCount === null ) {
        $cartDetails = $this->getClearCartDetails();
        $this->setCartDetails($cartDetails);
      }
      return (object) $cartDetails;
    }
  }

  public function setCartDetails( $data )
  {
    $this->setSessionData('fanplayr_cart', json_encode($data));
  }

  private function checkIfCartDetailsRefreshNeeded() {
    $forceRefresh = $this->getConfig('fanplayr/options/force_cart_refresh');
    $needsToRefresh = $forceRefresh || $this->getSessionData('fanplayr_update_cart_next_call');
    if ( $needsToRefresh ) {
      $this->setSessionData('fanplayr_update_cart_next_call', false);
      $this->refreshCartDetails();
    }
  }

  public function refreshCartDetailsNextCall()
  {
    $this->setSessionData('fanplayr_update_cart_next_call', true);
  }

  public function refreshCartDetails( $quote = null )
  {
    if ( !$quote ) {
      $objectManager = ObjectManager::getInstance();
      $cart = $objectManager->get('Magento\Checkout\Model\Cart');
      if ( $cart ) {
        $quote = $cart->getQuote();
      } else {
        $this->_logger->addError('Fanplayr: No cart available in Data::refreshCartDetails');
      }
    }

    if ( $quote ) {

      $products = array();

      $masterId = $this->getConfig('fanplayr/fanplayrMasterProductTrackingEnable/enabled');

      foreach ($quote->getAllVisibleItems() as $item) {

        // simple vs complex products
        if ($option = $item->getOptionByCode('simple_product')) {
          $p = $option->getProduct();
        } else {
          $p = $item->getProduct();
        }

        $products[] = array(
          'qty' => $item->getQty(),
          'price' => $item->getCalculationPrice(),
          'catName' => '',
          'id' => !$p ? '' : ($masterId ? $item->getProductId() : $p->getId()),
          'sku' => !$p ? '' : $p->getSku(),
          'name' => $item->getName(),
          'catId' => !$p ? '' : implode(',', $p->getCategoryIds())
        );
      }

      $subTotal = $quote->getBaseSubtotal();
      $total = $quote->getBaseSubtotalWithDiscount();
      $discount = $subTotal - $total;

      $cart = array(
        'lineItemCount' => $quote->getItemsCount(),
        'numItems' => $quote->getItemsQty(),

        'subTotal' => $subTotal,
        'total' => $total,
        'discount' => $discount,
        'discountCode' => $quote->getCouponCode(),

        'currency' => $quote->getQuoteCurrencyCode(),
        'products' =>  json_encode($products),

        'id' => $quote->getCustomerId(),
        'email' => $quote->getCustomerEmail()
        //'first' => $quote->getCustomerFirstname(),
        //'last' => $quote->getCustomerLastname()
      );

      $this->setCartDetails($cart);

    }
  }

}