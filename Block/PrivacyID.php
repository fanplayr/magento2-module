<?php

namespace Fanplayr\SmartAndTargeted\Block;

/**
 * INSTRUCTIONS
 * 
 * Require this file and use the "PrivacyID" class to handle requests, or
 * alternatively you can uncomment the lines below to use handle the request
 * directly in this file.
 */

// $api = new Fanplayr\PrivacyID();
// $api->siteKey = "YOUR_SITE_KEY";
// $api->siteSecret = "YOUR_SITE_SECRET";
// $api->handleRequest();

class PrivacyID {
  const ERROR_GENERAL = "001";
  const ERROR_VAR_SITE_KEY = "002";
  const ERROR_VAR_SITE_SECRET = "003";
  const ERROR_VAR_COOKIE_NAME = "004";
  const ERROR_VAR_COOKIE_MAX_AGE = "005";
  const ERROR_VAR_ENDPOINT = "006";
  const ERROR_API_REQUEST_TIMEOUT = "007";
  const ERROR_API_REQUEST_FAILED = "008";
  const ERROR_API_RESPONSE_PARSE_FAILED = "009";
  const ERROR_API_RESPONSE_INVALID = "010";

  public $endpoint = "https://api.privacyid.ai";
  /**
   * Uniquely identifies the site.
   */
  public $siteKey = null;
  /**
   * Secret used to authenticate API requests for the site.
   */
  public $siteSecret = null;
  /**
   * Name of cookie to store primary user key in. Defaults to "_fpuid".
   */
  public $cookieName = "_fpuid";
  /**
   * The maximum duration to store the cookie in milliseconds. Defaults to 1
   * year.
   */
  public $cookieMaxAgeMs = 365 * 24 * 60 * 60 * 1000;
  /**
   * The maximum duration to wait for an API response in seconds. Defaults
   * to 5 seconds.
   */
  public $requestTimeoutSecs = 5;

  function handleRequest() {
    header("Content-Type: text/plain");

    // Check for valid configuration variables.
    if (!isset($this->siteKey)) {
      return $this->outputError(self::ERROR_VAR_SITE_KEY);
    }
    if (!isset($this->siteSecret)) {
      return $this->outputError(self::ERROR_VAR_SITE_SECRET);
    }
    if (!isset($this->cookieName) ) {
      return $this->outputError(self::ERROR_VAR_COOKIE_NAME);
    }
    if (!isset($this->cookieMaxAgeMs) || $this->cookieMaxAgeMs <= 0) {
      return $this->outputError(self::ERROR_VAR_COOKIE_MAX_AGE);
    }
    if (!isset($this->endpoint)) {
      return $this->outputError(self::ERROR_VAR_ENDPOINT);
    }

    // Build the request url.
    $uri = new URI($this->endpoint);
    $uri->path = "/v2/identify";
    // Forward all GET parameters that are passed from the browser. This will
    // allow us to improve the browser API without requiring changes on the
    // server.
    foreach ($_GET as $k => $v) {
      $uri->query[$k] = $v;
    }
    $uri->query["siteKey"] = $this->siteKey;
    // Ensure we only send a SHA-256 hash of the userId to avoid transmitting
    // any PII.
    if (array_key_exists("userId", $uri->query)) {
      $uri->query["userId"] = hash("sha256", $uri->query["userId"]);
    }
    // Append userKey if it exists in the cookie.
    if (array_key_exists($this->cookieName, $_COOKIE)) {
      $uri->query["userKey"] = $_COOKIE[$this->cookieName];
    } 
    // Sign the request url  using the secret.
    $signed = $uri->sign($this->siteSecret);

    // Perform the request.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $signed);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, $this->requestTimeoutSecs);
    $output = curl_exec($ch);
    if (curl_errno($ch)) {
      // An error occured.
      $totalTimeSecs = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
      if ($totalTimeSecs > $this->requestTimeoutSecs) {
        // The request timed out.
        curl_close($ch);
        return $this->outputError(self::ERROR_API_REQUEST_TIMEOUT);
      }
      // Some other error occurred.
      curl_close($ch);
      return $this->outputError(self::ERROR_GENERAL);
    }
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($httpCode !== 200) {
      // Returned a bad status.
      curl_close($ch);
      return $this->outputError(self::ERROR_API_REQUEST_FAILED);
    }
    curl_close($ch);

    // Handle the response.
    $data = json_decode($output, true);
    if ($data === null) {
      return $this->outputError(self::ERROR_API_RESPONSE_PARSE_FAILED);
    }
    if ($data && $data["status"] === "SUCCESS") {
      $response = $data["response"];
      if (array_key_exists("userKey", $response)) {
        $userKey = urlencode($response["userKey"]);
        $cookieName = $this->cookieName;
        $cookieMaxAgeMs = $this->cookieMaxAgeMs;
        header("Set-Cookie: {$cookieName}={$userKey}; Path=/; Max-Age={$cookieMaxAgeMs}; HttpOnly; SameSite=Strict; Secure", false);
      }
      $output = array();
      if (array_key_exists("payload", $response)) {
        // Output the response payload exactly as is.
        $output = $response["payload"];
      }
      echo json_encode($output);
    } else {
      $this->outputError(self::ERROR_API_RESPONSE_INVALID);
    }
  }

  private function outputError($code) {
    echo json_encode(array(
      "error" => $code
    ));
  }
}

class URI {
  public $scheme = "";
  public $host = "";
  public $port = "";
  public $user = "";
  public $pass = "";
  public $path = "";
  public $query = array();
  public $fragment = "";

  function __construct($url) {
    $parts = parse_url($url);
    $this->scheme = array_key_exists("scheme", $parts) ? $parts["scheme"] : "";
    $this->host = array_key_exists("host", $parts) ? $parts["host"] : "";
    $this->port = array_key_exists("port", $parts) ? $parts["port"] : "";
    $this->user = array_key_exists("user", $parts) ? $parts["user"] : "";
    $this->pass = array_key_exists("pass", $parts) ? $parts["pass"] : "";
    $this->path = array_key_exists("path", $parts) ? $parts["path"] : "";
    $this->query = array_key_exists("query", $parts) ? $this->parseQuery($parts["query"]) : array();
    $this->fragment = array_key_exists("fragment", $parts) ? $parts["fragment"] : "";
  }

  function clone() {
    return new URI($this->build());
  }

  function __toString() {
    return $this->build();
  }

  function sign($siteSecret) {
    // Create a copy of the query to avoid mutating the original.
    $uri = $this->clone();
    $uri->query["timestamp"] = time() * 1000;
    unset($uri->query["signature"]);
    ksort($uri->query);
    $content = http_build_query($uri->query);
    // $uri->query["signature"] = hash_hmac("sha256", $content, $siteSecret);
    // return $uri->build();
    $copy = $this->clone();
    $copy->query["timestamp"] = $uri->query["timestamp"];
    $copy->query["signature"] = hash_hmac("sha256", $content, $siteSecret);
    return $copy->build();
  }

  public function build() {
    $url = "";
    if (!empty($this->scheme)) {
      $url .= $this->scheme . "://";
    }
    if (!empty($this->user)) {
      $url .= $this->user;
    }
    if (!empty($this->pass)) {
      $url .= $this->pass;
    }
    if (!empty($this->host)) {
      if (!empty($this->user) || !empty($this->pass)) {
        $url .= "@";
      }
      $url .= $this->host;
    }
    if (!empty($this->port)) {
      $url .= ":" . $this->port;
    }
    if (!empty($this->path)) {
      $url .= $this->path;
    }
    if (!empty($this->query)) {
      $url .= "?" . $this->buildQuery($this->query);
    }
    if (!empty($this->fragment)) {
      $url .= "#" . $this->fragment;
    }
    return $url;
  }

  private function parseQuery($queryStr) {
    $query = array();
    foreach (explode("&", $queryStr) as $part) {
      list($key, $value) = explode("=", $part);
      $query[$key] = $value;
    }
    return $query;
  }

  private function buildQuery($queryParts) {
    return http_build_query($queryParts, "", "&", PHP_QUERY_RFC3986);
  }
}